package com.proof.concept.mongodb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.proof.concept.mongodb.common.document.StudentDocument;
import com.proof.concept.mongodb.rest.to.ResponseRest;
import com.proof.concept.mongodb.service.StudentService;


@RestController
@RequestMapping(value = "/v1/student")
public class TestController {

	@Autowired
	StudentService studentService;
	
	@RequestMapping(value = "/listAll",method = RequestMethod.GET)
	ResponseRest<List<StudentDocument>> listAll() {
		ResponseRest<List<StudentDocument>> response = new ResponseRest<List<StudentDocument>>();
		response.setStatus(200);
		response.setResponse(studentService.findAll());
		return response;
	}
	
	@RequestMapping(value = "/",method = RequestMethod.POST)
	ResponseRest<StudentDocument> insert(@RequestBody StudentDocument studentDocument) {
		ResponseRest<StudentDocument> response = new ResponseRest<StudentDocument>();
		response.setStatus(200);
		studentDocument.setId(null);
		response.setResponse(studentService.save(studentDocument));
		return response;
	}
	
	@RequestMapping(value = "/",method = RequestMethod.PUT)
	ResponseRest<StudentDocument> update(@RequestBody StudentDocument studentDocument) {
		ResponseRest<StudentDocument> response = new ResponseRest<StudentDocument>();
		response.setStatus(200);
		response.setResponse(studentService.save(studentDocument));
		return response;
	}
	@RequestMapping(value = "/",method = RequestMethod.DELETE)
	ResponseRest<String> delete(@RequestBody String id) {
		ResponseRest<String> response = new ResponseRest<String>();
		studentService.delete(id);
		response.setStatus(200);
		response.setResponse("deleted");
		return response;
	}
	
	@RequestMapping(value = "/listByName",method = RequestMethod.POST)
	ResponseRest<List<StudentDocument>> listAll(@RequestBody String name) {
		System.out.println(name);
		ResponseRest<List<StudentDocument>> response = new ResponseRest<List<StudentDocument>>();
		response.setStatus(200);
		response.setResponse(studentService.findByName(name));
		return response;
	}
}
