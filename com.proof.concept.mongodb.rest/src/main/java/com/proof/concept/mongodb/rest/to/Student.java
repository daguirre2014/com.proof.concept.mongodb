package com.proof.concept.mongodb.rest.to;

public class Student {
	private String id;
	private String name;
	private boolean undergrad;
	private int units;
	private String[] classes;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isUndergrad() {
		return undergrad;
	}

	public void setUndergrad(boolean undergrad) {
		this.undergrad = undergrad;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public String[] getClasses() {
		return classes;
	}

	public void setClasses(String[] classes) {
		this.classes = classes;
	}
}
