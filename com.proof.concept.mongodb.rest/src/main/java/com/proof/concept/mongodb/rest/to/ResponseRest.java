package com.proof.concept.mongodb.rest.to;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseRest<T> {
	
	@JsonProperty(value="status")
	private int status;
	
	@JsonProperty(value="description")
	private String description;
	
	@JsonProperty(value="response")
	private T response;
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public T getResponse() {
		return response;
	}
	public void setResponse(T response) {
		this.response = response;
	}
	
	
}
