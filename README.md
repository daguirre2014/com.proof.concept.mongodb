# README #

Tutorial basico para instalar MongoDB StandAlone de manera local e implementarlo con Java y exponerlo como servicios

### Primeros pasos ###

 - Descarga la instancia de Mongo de: <br>
         https://www.mongodb.com/download-center?jmp=nav#community
         
 - En la ruta de instalacion en mi caso C:\Program Files\MongoDB\Server\3.4\bin crea el archivo mongod.cfg
 
 - Con las siguientes instrucciones basicas
	 systemLog:
		destination: file
		path: D:\mongodb\data\log\mongod.log
	storage:
		dbPath: D:\mongodb\data
        
 - Luego en la consola de windows ejecutar 
 	"C:\Program Files\MongoDB\Server\3.4\bin\mongod.exe" --config "C:\Program Files\MongoDB\Server\3.4\mongod.cfg" --install
    
 - Una vez que termine el proceso puedes ejecutar
 	net start MongoDB
	Y si quisieras detenerlo con
	net stop MongoDB
    
 - Hasta este punto ya tendrias tu instancia corriendo y podrias iniciar la consola de Mongo con:
 	"mongo"
    
	A partir de aqui ya podrias ejecutar comandos de en MongoDB

* Hay varios tipos de instalacion o incluso instarlo como servicio en windows que se podria revisar de aqui 
					https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/

### Dejo algunos comandos basicos para probar en MongoDB ###

* Crear un usuario 
db.createUser(
   {
     user: "testdb",
     pwd: "testdb",
     roles: [ "readWrite"]
   }
)

* Mostrar las instancias creadas
show dbs

* Usar una instancia o crearla sino existe
use test

* Lista las tablas o colecciones(NoSql)
show collections

* Crear una coleccion
db.createCollection("Student")

* Insertar un registro
db.Student.insert({
    name: 'Joe',
    undergrad: true,
    units: 9,
    classes: ['geography', 'math', 'journalism']
})

* Insertar varios en caso de que no exista la coleccion la crea 
for( var i=0 ; i< 10; i++ ){ db.things.insert({ x: i}) };

* Lista todos las inserciones de la coleccion
db.Student.find({})

* Lista por un atributo en especifico
db.Student.find({'name': 'Rachel'})

* Lista todas las coincidencias
db.Student.find({"name": {$regex : 'ev' }})

* Mayor que
db.Student.find({units: {$gt: 6}})

* Menor que
db.Student.find({units: {$lt: 7}})

* Inclusion
db.Student.find({classes: {$in: ['history']}}) - include

* Ascendente
db.Student.find({classes: {$in: ['history']}}).sort({units: -1})

* Descendente
db.Student.find({}).sort({name: 1})

* Limitar cantidad de registros
db.Student.find({}).sort({name: 1}).limit(2)