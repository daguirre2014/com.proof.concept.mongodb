package com.proof.concept.mongodb.common.document;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Student")
public class StudentDocument {
	@Id
	private String id;
	private String name;
	private boolean undergrad;
	private int units;
	private String[] classes;

	public StudentDocument() {
		super();
	}

	public StudentDocument(String name, boolean undergrad, int units, String[] classes) {
		super();
		this.name = name;
		this.undergrad = undergrad;
		this.units = units;
		this.classes = classes;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", name=" + name + ", undergrad=" + undergrad + ", units=" + units + ", classes="
				+ classes + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isUndergrad() {
		return undergrad;
	}

	public void setUndergrad(boolean undergrad) {
		this.undergrad = undergrad;
	}

	public int getUnits() {
		return units;
	}

	public void setUnits(int units) {
		this.units = units;
	}

	public String[] getClasses() {
		return classes;
	}

	public void setClasses(String[] classes) {
		this.classes = classes;
	}

}
