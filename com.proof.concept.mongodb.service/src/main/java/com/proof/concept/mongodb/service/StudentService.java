package com.proof.concept.mongodb.service;

import java.util.List;

import com.proof.concept.mongodb.common.document.StudentDocument;

public interface StudentService {

	List<StudentDocument> findAll();
	List<StudentDocument> findByName(String name);
	StudentDocument save(StudentDocument student);
	void delete (String id);
}
