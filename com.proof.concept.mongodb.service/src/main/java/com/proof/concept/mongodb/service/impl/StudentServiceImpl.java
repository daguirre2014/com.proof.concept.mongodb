package com.proof.concept.mongodb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proof.concept.mongodb.common.document.StudentDocument;
import com.proof.concept.mongodb.dao.repository.StudentRepositoryBase;
import com.proof.concept.mongodb.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	StudentRepositoryBase studentRepository;
	
	@Override
	public List<StudentDocument> findAll() {
		return studentRepository.findAll();
	}

	@Override
	public List<StudentDocument> findByName(String name) {
		return studentRepository.findByName(name);
	}

	@Override
	public StudentDocument save(StudentDocument student) {
		return studentRepository.save(student);
	}

	@Override
	public void delete(String id) {
		System.out.println("entro a borrar con el key " + id);
		studentRepository.delete(id);
		System.out.println("borro con el key " + id);
	}

}
