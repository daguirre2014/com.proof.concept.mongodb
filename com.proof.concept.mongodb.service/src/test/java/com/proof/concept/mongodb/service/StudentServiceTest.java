package com.proof.concept.mongodb.service;


import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.proof.concept.mongodb.common.document.StudentDocument;
import com.proof.concept.mongodb.dao.AppDao;
import com.proof.concept.mongodb.dao.repository.StudentRepositoryBase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ServiceApp.class)
@PropertySource("classpath:application.properties")
public class StudentServiceTest {
	
	@Autowired
	StudentService studentService;

	@BeforeClass
	public static void oneTimeSetUp() {
		// one-time initialization code
		System.out.println("@BeforeClass - oneTimeSetUp");
	}

	@AfterClass
	public static void oneTimeTearDown() {
		// one-time cleanup code
		System.out.println("@AfterClass - oneTimeTearDown");
	}

	@Before
	public void setUp() {
		System.out.println("@Before - setUp");
	}

	@After
	public void tearDown() {
		System.out.println("@After - tearDown");
	}

	@Test
	public void listAllTest() {
		List<StudentDocument> list = studentService.findAll(); 
		list.forEach((student) -> System.out.println(student.toString()));
		assertNotNull(list);
	}
}
