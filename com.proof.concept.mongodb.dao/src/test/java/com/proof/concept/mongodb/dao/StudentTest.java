package com.proof.concept.mongodb.dao;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.proof.concept.mongodb.common.document.StudentDocument;
import com.proof.concept.mongodb.dao.repository.StudentRepositoryBase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AppDao.class)
@PropertySource("classpath:application.properties")
public class StudentTest {

	@Autowired
	StudentRepositoryBase studentRepository;

	@BeforeClass
	public static void oneTimeSetUp() {
		 
		System.out.println("@BeforeClass - oneTimeSetUp");
	}

	@AfterClass
	public static void oneTimeTearDown() {
		// one-time cleanup code
		System.out.println("@AfterClass - oneTimeTearDown");
	}

	@Before
	public void setUp() {
		System.out.println("@Before - setUp");
	}

	@After
	public void tearDown() {
		System.out.println("@After - tearDown");
	}

	@Test
	public void listAllTest() {
		List<StudentDocument> list = studentRepository.findAll(); 
		list.forEach((student) -> System.out.println(student.toString()));
		assertNotNull(list);
	}
	
	@Test
	public void saveTest() {
		StudentDocument student = studentRepository.save(new StudentDocument("asa",true, 12, null)); 
		 System.out.println(student.toString());
		assertNotNull(student);
	}
	
	@Test
	public void deleteTest() {
		studentRepository.delete("59b9c05d769d53868c9f688c"); 
	}
}
