package com.proof.concept.mongodb.dao.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.proof.concept.mongodb.common.document.StudentDocument;
import com.proof.concept.mongodb.dao.repository.StudentRepositoryCustom;

@Repository
public class StudentRepositoryCustomImpl implements StudentRepositoryCustom {

	@Autowired
	MongoTemplate mongoTemplate;

	//@Override
	public int updateStudent(StudentDocument student) {
		 Query query = new Query(Criteria.where("id").is(student.getId()));
		 Update update = new Update();
		 
		return 0;
	}
}
