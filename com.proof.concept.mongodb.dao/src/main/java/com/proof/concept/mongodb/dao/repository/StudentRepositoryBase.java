package com.proof.concept.mongodb.dao.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import com.proof.concept.mongodb.common.document.StudentDocument;

@Repository
public interface StudentRepositoryBase extends MongoRepository<StudentDocument, String>,StudentRepositoryCustom {

	@Query("{name:{$regex : '?0'}}")
	List<StudentDocument> findByName(String name);

}
